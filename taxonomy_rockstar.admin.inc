<?php
/**
 * Administration settings form
 *
 * @return $form
 */
function taxonomy_rockstar_help($path, $arg) {
  switch ($path) {
    case 'admin/help#taxonomy-rockstar':
    case 'admin/content/taxonomy-rockstar':
      $output = '<p>'. t('The <a href="http://drupal.org/project/taxonomy_rockstar"><strong>Taxonomy Rockstar API</strong></a> module provides an advanced presentation layer with limitless configurability based on specific vocabularies, specific node types, and even based on the structure of the taxonomy tree being rendered. The purpose of this module is to allow users more powerful ways to navigate content through taxonomy. The Drupal taxonomy module provides amazing flexibility, but without combining the efforts of dozens of taxonomy modules, or custom code it is difficult for standard users to create an interface for their users to browse content quickly and easily based on the categories that were chosen to classify said content.') .'</p>';
      $output .= '<p>'. t('<strong>Taxonomy Rockstar</strong> module comes with an assortment of addon modules to enable additional functionality. Those modules can be enabled or disabled from the <a href="admin/build/modules"><em>modules page</em></a> in your Drupal installation. If you have questions or issues regarding <strong>Taxonomy Rockstar</strong> you may visit the <a href="http://drupal.org/project/taxonomy_rockstar">project page</a> at <a href="http://drupal.org">drupal.org</a>, or <a href="http://drupal.org/project/issues/taxonomy_rockstar">browse</a> or <a href="http://drupal.org/node/add/project-issue/taxonomy_rockstar">submit issues</a> using the issue queue.') .'</p>';
      break;
    case 'admin/content/taxonomy-rockstar/modules':
      $output = '<p>'. t('The modules section allows you to adjust module specific settings related to Taxonomy Rockstar\'s sub-modules. You are able to customize some of these features on a per-vocabulary basis as well on the edit vocabulary page.') .'</p>';
      break;
    case 'admin/content/taxonomy-rockstar/layouts':
      $output = '<p>'. t('The Layouts Section gives you access and exaples of the default layouts available to you based on enabled sub-modules, and the type of taxonomy trees that you are rendering views for.') .'</p>';
      break;
  }
  return $output;
}



function taxonomy_rockstar_admin_settings() {
  $form['tr'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Features & Settings'),
    '#description' => t('General settings that will change the usage and/or appearance of the <a href="http://drupal.org/project/taxonomy_rockstar">Taxonomy Rockstar</a> module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $vocab_options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vid => $vocab) {
    $vocab_options[$vid] = $vocab->name;
  }
  $form['tr']['tr_vocabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Apply to Vocabularies'),
    '#description' => t('Here you can select which vocabularies will be affected by Taxonomy Rockstar interfaces.'),
    '#default_value' => variable_get('tr_vocabs', array()),
    '#options' => $vocab_options,
  );
  $form['tr']['tr_jquery'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable jQuery Interfaces'),
    '#description' => t('By selecting this option, several interface features will be enhanced for the end user browsing taxonomy trees.'),
    '#default_value' => variable_get('tr_jquery', array()),
    '#options' => array('enabled' => 'Yes, enable fancy jQuery features!'),
  );
  $form['#submit'] = array('taxonomy_rockstar_admin_settings_submit');
  return system_settings_form($form);
}
function taxonomy_rockstar_layout_settings() {
	$form['tr'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout Configuration'),
    '#description' => t('Manage and configure layouts of your taxonomy pages. From this section, you may select customized options for each vocabulary below.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tr']['tr-used-layouts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Layouts'),
    '#description' => t('You may choose to enable/disable layouts that are by default enabled by sub modules, and exclude them from the specific vocabulary setting pages. This will prevent admins from selecting one of those layout options for a vocabulary, AND will not allow it to be chosen by users who have the ability to modify the filter/interface. <em>If NO options are selected here, it will be assumed you do NOT want to filter these layout types</em>'),
    '#required' => FALSE,
    '#default_value' => variable_get('tr-used-layouts', array()),
    '#options' => taxonomy_rockstar_generate_layouts(FALSE),
  );
  return system_settings_form($form);
}
function taxonomy_rockstar_layout_configurations() {
	$vocabularies = taxonomy_get_vocabularies();
	$vocab = $vocabularies[arg(4)];
  //krumo($vocab);
	$form['tr'][$vocab->vid] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout Options for '. $vocab->name),
    '#description' => t('Manage and configure layouts for the vocabulary titled <strong>'. $vocab->name .'</strong>.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tr'][$vocab->vid]['tr-layout-'. $vocab->vid] = array(
    '#type' => 'radios',
    '#title' => t('Layout Type for '. $vocab->name),
    '#description' => t('Selecting an option here will make this layout the default rendering type for the specified vocabulary.'),
    '#required' => TRUE,
    '#default_value' => variable_get('tr-layout-'. $vocab->vid, array()),
    '#options' => taxonomy_rockstar_generate_layouts($vocab->vid),
  );
  return system_settings_form($form);
}
function taxonomy_rockstar_module_settings() {
	$form['tr-list'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML Lists'),
    '#description' => t('Manage and configure layouts using default HTML lists.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  if(!module_exists('taxonomy_rockstar_list')) {
  	$form['tr-list']['not-installed'] = array(
		  '#value' => t('<div class="messages warning tr-module-missing"><h4>HTML List module not installed</h4>'). t('<p>The Taxonomy Rockstar Lists module is not enabled, so features here are not available. You may enable this module by visiting the <a href="'.base_path().'admin/build/modules"><strong>modules page</strong></a> and enabling it. You can find it in the Taxnonomy Rockstar section.</p></div>'),
		);
  }
  else {
  	// include the settings form from the taxonomy_rockstar_list.admin.inc file
  }
  $form['tr-checkbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Checkbox Forms'),
    '#description' => t('Manage and configure layouts using checkbox search navigation.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  if(!module_exists('taxonomy_rockstar_checkbox')) {
    $form['tr-checkbox']['not-installed'] = array(
      '#value' => t('<div class="messages warning tr-module-missing"><h4>Checkbox module not installed</h4>'). t('<p>The Taxonomy Rockstar Checkboxes module is not enabled, so features here are not available. You may enable this module by visiting the <a href="'.base_path().'admin/build/modules"><strong>modules page</strong></a> and enabling it. You can find it in the Taxnonomy Rockstar section.</p></div>'),
    );
  }
  else {
    // include the settings form from the taxonomy_rockstar_list.admin.inc file
  }
  $form['tr-radio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Radio Forms'),
    '#description' => t('Manage and configure layouts using radio search navigation.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  if(!module_exists('taxonomy_rockstar_radio')) {
    $form['tr-radio']['not-installed'] = array(
      '#value' => t('<div class="messages warning tr-module-missing"><h4>Radio module not installed</h4>'). t('<p>The Taxonomy Rockstar Radios module is not enabled, so features here are not available. You may enable this module by visiting the <a href="'.base_path().'admin/build/modules"><strong>modules page</strong></a> and enabling it. You can find it in the Taxnonomy Rockstar section.</p></div>'),
    );
  }
  else {
    // include the settings form from the taxonomy_rockstar_list.admin.inc file
  }
  $form['tr-select'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select Forms'),
    '#description' => t('Manage and configure layouts using select menu search navigation.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  if(!module_exists('taxonomy_rockstar_select')) {
    $form['tr-select']['not-installed'] = array(
      '#value' => t('<div class="messages warning tr-module-missing"><h4>Select module not installed</h4>'). t('<p>The Taxonomy Rockstar Select Menus module is not enabled, so features here are not available. You may enable this module by visiting the <a href="'.base_path().'admin/build/modules"><strong>modules page</strong></a> and enabling it. You can find it in the Taxnonomy Rockstar section.</p></div>'),
    );
  }
  else {
    // include the settings form from the taxonomy_rockstar_list.admin.inc file
  }
  return system_settings_form($form);
}

/**
 * Create a thumbnail that links to screen shots of view modes.
 *
 * @param $view
 *   Type: string; The view to create the thumbnail for.
 * @param $title
 *   Type: string; The thumbnail's title/alternate text.
 *
 * @return string
 */
function taxonomy_rockstar_screenshot_link($view, $title) {
  return l(theme_image(drupal_get_path('module', 'taxonomy_rockstar') . '/images/thumb_' . $view . '.gif', $title, $title), 'http://drupal.org/files/images/screenshot_' . $view . '.preview.png', array('html' => TRUE));
}
/**
 * Submit Handlers
 */
function taxonomy_rockstar_admin_settings_submit() {
	// must clear menu cache to rebuild the sub-menu for the Layouts page
	menu_rebuild();
}
