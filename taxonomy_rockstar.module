<?php 
/**
 * @file taxonomy_rockstar.module provides core API functions for taxonomy tree browsing and searching
 * @author Jake Strawn - http://himerus.com
 */
/**
 * Implementation of hook_init().
 */
function taxonomy_rockstar_init() {
  // add our custom javascript & CSS
  drupal_add_js(drupal_get_path('module', 'taxonomy_rockstar') .'/taxonomy_rockstar.js', 'module', 'header', TRUE);
  drupal_add_css(drupal_get_path('module', 'taxonomy_rockstar') .'/taxonomy_rockstar.css', 'module', 'all', FALSE); 
}
function taxonomy_rockstar_perm() {
	return array('administer taxonomy rockstar');
}
/**
 * Implementation of hook_menu().
 */
function taxonomy_rockstar_menu() {
  $items = array();
  $items['admin/content/taxonomy-rockstar'] = array(
    'title' => t('Taxonomy Rockstar'),
    'description' => t('Configure Taxonomy Rockstar Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_rockstar_admin_settings', arg(0)),
    'access arguments' => array('administer taxonomy rockstar'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'taxonomy_rockstar.admin.inc',
  );
  $items['admin/content/taxonomy-rockstar/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/content/taxonomy-rockstar/layouts'] = array(
    'title' => t('Layouts'),
    'description' => t('Manage custom layouts for your taxonomy vocabularies'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_rockstar_layout_settings'),
    'access arguments' => array('administer taxonomy rockstar'),
    'type' => MENU_LOCAL_TASK,
    'parent' => 'admin/content/taxonomy-rockstar',
    'file' => 'taxonomy_rockstar.admin.inc',
  );
  $items['admin/content/taxonomy-rockstar/layouts/global'] = array(
    'title' => 'Global settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  // generate sub-menus for the layout page for each taxonomy vocab enabled
  $active_vocabs = variable_get('tr_vocabs', array());
  //drupal_set_message(krumo($active_vocabs));
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vid => $vocab) {
    if ($active_vocabs[$vid]) {
      $items['admin/content/taxonomy-rockstar/layouts/'. $vocab->vid] = array(
		    'title' => t($vocab->name),
		    'description' => t('Manage custom layouts for the vocabulary titled '. $vocab->name),
		    'page callback' => 'drupal_get_form',
		    'page arguments' => array('taxonomy_rockstar_layout_configurations'),
		    'access arguments' => array('administer taxonomy rockstar'),
		    'type' => MENU_LOCAL_TASK,
		    'parent' => 'admin/content/taxonomy-rockstar/layouts',
		    'file' => 'taxonomy_rockstar.admin.inc',
		  );
    }
  }
  $items['admin/content/taxonomy-rockstar/modules'] = array(
    'title' => t('Modules'),
    'description' => t('Manage sub-module settings for Taxonomy Rockstar'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_rockstar_module_settings'),
    'access arguments' => array('administer taxonomy rockstar'),
    'type' => MENU_LOCAL_TASK,
    'parent' => 'admin/content/taxonomy-rockstar',
    'file' => 'taxonomy_rockstar.admin.inc',
  );
  
  return $items;
}
/**
 * Taxonomy Rockstar Hooks
 */

/**
 * Implementation of hook_define_layouts()
 * This function takes only one param, which is the vocabulary id (optional)
 * If $vid is provided, it will be presenting on a vocab specific page, and will
 * filter out the options that have not been enabled on the global settings page
 * 
 * @example 
 
 * function taxonomy_rockstar_select_define_layouts() {
 *   return array(
 *     'select-basic' => 'Basic Select Filter',
 *     'select-multiple' => 'Multiple Select Menu(s)',
 *     'select-chained' => 'Chained Select Filter',
 *     'select-hybrid' => 'Hybrid Select Filter',
 *   );
 * }
 * 
 * @param numeric $vid
 * @return array of options to render in admin forms
 */

function taxonomy_rockstar_generate_layouts($vid = 0) {
  /**
   * Get all layouts provided by hook_define_layouts()
   */
  $all_layouts = module_invoke_all('define_layouts');
  /**
   * If we are on a specific vocab page implementing this, we will disable the options that
   * haven't been turned on with the Global settings page.
   */
  if ($vid && array_filter(variable_get('tr-used-layouts', array()))) {
    $available_layouts = array_filter(variable_get('tr-used-layouts', array()));
    $print_layouts = array_intersect_key($all_layouts, $available_layouts);
    return $print_layouts;
  }
  else {
    return $all_layouts;
  }
}