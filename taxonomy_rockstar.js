$(document).ready(function(){
	// custom submit handler for our form
	$('#tr .form-submit').css('display', 'none');
	// will only be used for javascript aware browsers. Users without javascript will have to submit the form themselves.
	$('#tr input, #tr option').click(function(){
		if($(this).val() != 0) {
			$('#tr').submit();
		}
	});
	// resets radio menu on back button type events
	$('#tr input:radio').attr('checked', false);
	// resets select menus on back button type events
	$('#tr select').val(0);
});